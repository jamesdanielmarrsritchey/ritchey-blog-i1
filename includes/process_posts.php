<?php
#Process posts
$location = realpath(dirname(__FILE__, 2));
eval(@substr(@file_get_contents("{$location}/evals/global_variables.php"), 5, -2));
###Get a list of all '.html_include' files in /www/private/posts
####If /www/private/posts doesn't exist, create it.
if (@is_dir("{$location}/www/private/posts") === FALSE){
	@mkdir("{$location}/www/private/posts/", 0777, TRUE);
}
####If /www/private/processed_posts doesn't exist, create it.
if (@is_dir("{$location}/www/private/processed_posts") === FALSE){
	@mkdir("{$location}/www/private/processed_posts/", 0777, TRUE);
}
###Rename, and move posts from /www/private/posts to /www/private/processed_posts
$files = @scandir("{$location}/www/private/posts");
if (@empty($files) === FALSE){
	$i = 1;
	foreach ($files as $file){
		if (@is_file("{$location}/www/private/posts/{$file}") === TRUE){
			####Determine the file extension, if any
			$file_extension = @strrchr($file, '.');
			if ($file_extension === FALSE){
				$file_extension = '';
			}
			if ($file_extension === '.html_include'){
				####Get timestamp from post_date element, if present.
				$time_stamp = FALSE;
				@preg_match('/<div id=\'post_date\'.*?<\/div>/', file_get_contents("{$location}/www/private/posts/{$file}"), $matches, PREG_OFFSET_CAPTURE);
				if (@isset($matches[0]) === TRUE){
					$matches = $matches[0];
					if (@isset($matches[0]) === TRUE){
						$matches = @preg_replace("/<div id=\'post_date\'.*?>/", "", $matches[0]);
						$matches = @preg_replace("/<\/div>/", "", $matches);
						$check1 = $matches;
						$time_stamp = @preg_replace("/[^0-9]/", "", $matches);
						if ($check1 !== $time_stamp){
							$time_stamp = FALSE;
						}
					}
				}
				if (@ctype_digit($time_stamp) === TRUE){
					####Move, and rename
					$catch = @rename("{$location}/www/private/posts/{$file}", "{$location}/www/private/processed_posts/{$time_stamp}.html_include");
					if ($catch === FALSE){
						#Do nothing. The original post will still be present for re-processing next time.
					}
				}
			}
		}
	}
}
###Generate copies of items in /www/private/processed_posts with full page HTML, and write to /public/posts. Overwrite existing posts with the same name.
$files = @scandir("{$location}/www/private/processed_posts");
###Generate full html page of it in /public/posts
if (@empty($files) === FALSE){
	$i = 1;
	foreach ($files as $file){
		####If /www/public/posts doesn't exist, create it.
		if (@is_dir("{$location}{$public}/posts") === FALSE){
			@mkdir("{$location}{$public}/posts/", 0777, TRUE);
		}
		if (@is_file("{$location}/www/private/processed_posts/{$file}") === TRUE){
			####Determine the file extension, if any
			$file_extension = @strrchr($file, '.');
			if ($file_extension === FALSE){
				$file_extension = '';
			}
			####Determine the filename, excluding the file extension
			$file_name_excluding_extension = @basename($file, $file_extension);
			if ($file_name_excluding_extension === FALSE){
				$file_name_excluding_extension = "filename_failed_{$i}";
			}
			if ($file_extension === '.html_include'){
				####Create full html page for post in /public/posts
				$page = $file_name_excluding_extension;
				@preg_match('/<h1 class=\'post_name\'.*?<\/h1>/', file_get_contents("{$location}/www/private/processed_posts/{$file}"), $matches, PREG_OFFSET_CAPTURE);
				if (@isset($matches[0]) === TRUE){
					$matches = $matches[0];
					if (@isset($matches[0]) === TRUE){
						$matches = @preg_replace("/<h1 class=\'post_name\'.*?>/", "", $matches[0]);
						$matches = @preg_replace("/<\/h1>/", "", $matches);
						$page = @preg_replace("/[^a-zA-Z0-9] /", "", $matches);
					}
				}
				$content = @file_get_contents("{$location}/www/private/processed_posts/{$file}");
				eval(@substr(@file_get_contents("{$location}/evals/html_layout.php"), 5, -2));
				@file_put_contents("{$location}{$public}/posts/{$file_name_excluding_extension}.html", $data);
			}
		}
	}
}
?>
