<?php
#Task
##Run templates
$location = realpath(dirname(__FILE__));
require_once $location . '/templates/robots.txt.php';
require_once $location . '/templates/index.html.php';
require_once $location . '/templates/licensing.html.php';
require_once $location . '/templates/terms_of_service.html.php';
require_once $location . '/templates/privacy_policy.html.php';
##Process uploads
$location = realpath(dirname(__FILE__));
require_once $location . '/includes/process_uploads.php';
##Process posts
$location = realpath(dirname(__FILE__));
require_once $location . '/includes/process_posts.php';
##Creates databases
$location = realpath(dirname(__FILE__));
require_once $location . '/includes/create_database1.php';
?>