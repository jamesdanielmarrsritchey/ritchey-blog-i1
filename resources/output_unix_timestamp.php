<?php
#Name:Output Unix Timestamp i1 v1
#Description:Outputs the current Unix timestamp for UTC.
#Usage:'php /resources/output_unix_timestamp.php'
$timestamp = time();
echo "{$timestamp}\n";
?>
