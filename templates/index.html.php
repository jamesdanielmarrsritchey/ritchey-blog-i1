<?php
#Template for /public/index.html
#Task
##Data
$location = realpath(dirname(__FILE__, 2));
eval(@substr(@file_get_contents("{$location}/evals/global_variables.php"), 5, -2));
$page = 'Blog Index';
$content = <<<'NOWDOC'
<div id='items'>

</div>
<script>
//Get URL Parameters
var url_parameters = new URLSearchParams(location.search);
var last_item = url_parameters.get("last_item");
var last_item = parseInt(last_item);
if (isNaN(last_item) === true){
	last_item = 0;
}
var item_quantity = url_parameters.get("item_quantity");
var item_quantity = parseInt(item_quantity);
if (isNaN(item_quantity) === true){
	item_quantity = 1;
}
var item_direction = url_parameters.get("item_direction");
if (item_direction == 'forward'){
	//Do nothing
} else if (item_direction == 'backward'){
	//Do nothing
} else {
	item_direction = 'forward';
}
var item_tags = url_parameters.get("item_tags");
var regex_pattern = /^[a-zA-Z0-9,\s]+$/i;
if (regex_pattern.test(item_tags) === false){
	item_tags = null;
}
if (item_tags == ''){
	var item_tags = null;
} else if (item_tags === null){
	//Do nothing
} else {
	var item_tags = item_tags.split(",");
}
var item_query = url_parameters.get("item_query");
var regex_pattern = /^[a-zA-Z0-9\s]+$/i;
if (regex_pattern.test(item_query) === false){
	item_query = null;
}
if (item_query == ''){
	var item_query = null;
} else if (item_query === null){
	//Do nothing
} else {
	var item_query = item_query.split(" ");
}
//Load database entries for items, and write data to page
if (item_direction == 'forward'){
	var current_item = last_item + 1;
} else {
	var current_item = last_item - 1;
}
var item_quantity_written = 0;
if (current_item > 0){
	var item = document.createElement("script");
	item.src = './databases/database1/' + current_item + '.js';
	document.getElementById("items").appendChild(item);
}
</script>
NOWDOC;
eval(@substr(@file_get_contents("{$location}/evals/html_layout.php"), 5, -2));
##Write data
@file_put_contents("{$location}{$public}/index.html", $data);
?>