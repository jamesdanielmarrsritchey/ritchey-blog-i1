<?php
$data = <<<HEREDOC
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<title>{$page}</title>
<style>
/* Defaults */
body {
	margin: 0px;
	background-color: #F2F2F2;
	font-size 16px;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
blockquote {
	padding: 10px;
	background-color: #D5D5D5;
	margin: 10px;
}

/* Header */
#header1 {
	background-color: #8892BF;
	color: #000000;
	text-align: center;
	border-color: #4F5B93;
	border-bottom-width: 5px;
	border-bottom-style: solid;
	padding-top: 10px;
	padding-bottom: 10px;
	box-shadow: 0 5px 5px #cccccc;
}
@media screen and (min-width: 1024px) {
	#header1 {
		text-align: left;
	}
}
#header1_sub1 {
	display: block;
	text-align: center;
	font-size: 22px;
	vertical-align: middle;
	line-height: 38px;
	color: #000000;
	text-shadow: 0px -1px 0px #AEAEAE;
}
@media screen and (min-width: 1024px) {
	#header1_sub1 {
		display: inline-block;
	}
}
.site_name:link {
	margin-left: 10px;
	margin-right: 10px;
	display: block;
	padding: 0px;
	color: #000000;
	text-decoration: none;
	text-shadow: 0px -1px 0px #AEAEAE;
}
.site_name:visited {
  color: #000000;
  text-decoration: none;
}
.site_name:hover {
  color: #333333;
  text-decoration: none;
}
.site_name:active {
  color: #000000;
  text-decoration: none;
}
#header1_sub2 {
	text-align: center;
	display: block;
}
@media screen and (min-width: 1024px) {
	#header1_sub2 {
		display: inline-block;
	}
}
.navigation:link {
	background-color: #CBD3F5;
	color: #111111;
	border-style: solid;
	border-width: 1px;
	border-color: #111111;
	display: block;
	min-width: calc(100% - 42px);
	max-width: calc(100% - 42px);
	padding: 10px;
	margin-left: 10px;
	margin-right: 10px;
}

/* Article */
#article1 {
	background-color: #FFFFFF;
	color: #111111;
	margin-left: 15px;
	margin-right: 15px;
	margin-top: 50px;
	min-height: 50vh;
	padding: 10px;
}
@media screen and (min-width: 1024px) {
	#article1 {
		max-width: 75%;
		min-width: 75%;
		margin-left: auto;
		margin-right: auto;
	}
}

/* Footer1 */
#footer1 {
	color: #7b7b7b;
	background-color: #F2F2F2;
	text-align: center;
	padding-top: 10px;
	padding-bottom: 10px;
	font-size: 16px;
	margin-top: 50px;
	min-width: 100%;
	max-width: 100%;
	border-top-style: solid;
	border-top-width: 1px;
	border-top-color: #e8e8e8;
}
.footer1_link:link {
	color: #7b7b7b;
	text-decoration: none;
}
.footer1_link:visited {
	color: #7b7b7b;
	text-decoration: none;
}
.footer1_link:hover {
	color: #0427df;
	text-decoration: none;
}
.footer1_link:active {
	color: #7b7b7b;
	text-decoration: none;
}

#footer1_sub1 {
	text-align: center;
	display: block;
}
@media screen and (min-width: 1024px) {
	#footer1_sub1 {
		display: inline-block;
	}
}
.secondary_link {
	display: inline-block;
	background-color: #DBE1FB;
	padding: 10px;
	min-width: calc(100% - 40px);
	max-width: calc(100% - 40px);
	margin-left: 10px;
	margin-right: 10px;
	margin-bottom: 10px;
}
@media screen and (min-width: 1024px) {
	.secondary_link {
		min-width: unset;
		max-width: unset;
		margin-left: unset;
		margin-bottom: unset;
	}
}
.secondary_link:link {
	text-decoration: none;
	color: #7b7b7b;
}
.secondary_link:visited {
	text-decoration: none;
	color: #7b7b7b;
}
.secondary_link:hover {
	text-decoration: none;
	color: #111111;
	background-color: #CBD3F5;
}
.secondary_link:active {
	text-decoration: none;
	color: #7b7b7b;
}

/* Footer2 */
#footer2 {
	color: #7b7b7b;
	background-color: #F2F2F2;
	text-align: right;
	padding-top: 10px;
	padding-bottom: 10px;
	font-size: 16px;
	margin-top: 10px;
	min-width: 100%;
	max-width: 100%;
}
.goto_header1:link {
	border: none;
	outline: none;
	font-size: 44px;
	line-height: 60px;
	font-weight: normal;
	color: #FFFFFF;
	background-color: #b3b3b3;
	border-radius: 35px;
	display: inline-block;
	min-width: 60px;
	min-height: 60px;
	margin-left: 10px;
	margin-right: 10px;
	margin-top: 10px;
	margin-bottom: 10px;
	text-align: center;
	text-decoration: none;
}
.goto_header1:visited {
  color: #FFFFFF;
  text-decoration: none;
}
.goto_header1:hover {
  color: #FFFFFF;
  text-decoration: none;
  background-color: #383838;
}
.goto_header1:active {
  color: #FFFFFF;
  text-decoration: none;
}
/* Entries */
.entry_icon {
	width: 48px;
	height: 48px;
}

/* Posts */
.post_name {
	text-align: center;
}
.post_tags {
	text-align: center;
	padding-left: 0px;
}
.post_tags li {
	display: inline-block;
    background-color: #cccccc;
    padding: 10px;
    margin-right: 5px;
    margin-bottom: 5px;
    margin-top: 5px;
    text-align: center;
}
#post_date {
	text-align: center;
}

/* Pages */
#page_name {
	text-align: center;
}
</style>
<script>
function ritchy_convert_timestamp_to_date_i1_v1() {
	//Get timestamp from post_date element
	var timestamp = document.getElementById("post_date").innerHTML;
	//Check that value only contains numbers
	if (isNaN(timestamp) === false){
		//Multiply by 1000 to convert the timestamp to milliseconds, as expected by Javascript
		var miliseconds_timestamp = parseInt(timestamp) * 1000;
		//Create a date object
		var date = new Date(miliseconds_timestamp);
		//Get the date in Month Day, Year format. Use UTC timezone.
		var month = date.getUTCMonth();
		if (month == '0'){
			var human_readable_month = 'January';
		} else if (month == '1'){
			var human_readable_month = 'February';
		} else if (month == '2'){
			var human_readable_month = 'March';
		} else if (month == '3'){
			var human_readable_month = 'April';
		} else if (month == '4'){
			var human_readable_month = 'May';
		} else if (month == '5'){
			var human_readable_month = 'June';
		} else if (month == '6'){
			var human_readable_month = 'July';
		} else if (month == '7'){
			var human_readable_month = 'August';
		} else if (month == '8'){
			var human_readable_month = 'September';
		} else if (month == '9'){
			var human_readable_month = 'October';
		} else if (month == '10'){
			var human_readable_month = 'November';
		} else if (month == '11'){
			var human_readable_month = 'December';
		} else {
			var human_readable_month = month.toString();
		}
		var day = date.getUTCDate();
		var day = day.toString();
		var year = date.getUTCFullYear();
		var year = year.toString();
		var human_readable_date = human_readable_month.concat(' ').concat(day).concat(', ').concat(year);
		//Update page
		document.getElementById("post_date").innerHTML = timestamp.concat(' (').concat(human_readable_date).concat(')');
	}
}
</script>
</head>
<body>
<div id='header1'>
<div id='header1_sub1'><a class="site_name" target="_self" href="{$address}/index.html">{$site_name}</a></div>
<div id='header1_sub2'><a class="navigation" target="_self" href="{$address}/navigation.html">☰ Navigation</a></div>
</div>
<div id='article1'>
{$content}
</div>
<div id='footer1'>
<p><a class='footer1_link' target="_self" href="https://laws-lois.justice.gc.ca/eng/acts/C-42/Index.html">Copyright &#169</a></p>
<div id='footer1_sub1'>
<a class='secondary_link' target="_self" href="{$address}/pages/licensing.html">Licensing</a>
<a class='secondary_link' target="_self" href="{$address}/pages/terms-of-service.html">Terms of Service</a>
<a class='secondary_link' target="_self" href="{$address}/pages/privacy-policy.html">Privacy Policy</a>
</div>
</div>
<div id='footer2'>
<a class="goto_header1" target="_self" href="#header1">&uArr;</a>
</div>
<script>
ritchy_convert_timestamp_to_date_i1_v1();
</script>
</body>
</html>
HEREDOC;
?>