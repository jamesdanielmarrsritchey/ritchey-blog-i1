<?php
$location = realpath(dirname(__FILE__, 2));
eval(@substr(@file_get_contents("{$location}/evals/global_variables.php"), 5, -2));
##Ensure directory exists
if (@is_dir("{$location}{$public}/databases") === FALSE){
	@mkdir("{$location}{$public}/databases/", 0777, TRUE);
}
if (@is_dir("{$location}{$public}/databases/database1") === FALSE){
	@mkdir("{$location}{$public}/databases/database1/", 0777, TRUE);
}
##Delete existing database file
$files = @scandir("{$location}{$public}/databases/database1");
foreach ($files as $file){
	if ($file !== '.'){
		if ($file !== '..'){
			@unlink("{$location}{$public}/databases/database1/{$file}");
		}
	}
}
##Create database
$files = @scandir("{$location}{$public}/posts");
$i = 0;
foreach ($files as $file){
	if ($file !== '.'){
		if ($file !== '..'){
			$i++;
			###Get the file_name
			$file_name = $file;
			###Get the post_name. If not found, set to file id instead.
			$file_id = @explode('.', $file_name, 2);
			$file_id = $file_id[0];
			$post_name = $file_id;
			@preg_match('/<h1 class=\'post_name\'.*?<\/h1>/', file_get_contents("{$location}{$public}/posts/{$file}"), $matches, PREG_OFFSET_CAPTURE);
			if (@isset($matches[0]) === TRUE){
				$matches = $matches[0];
				if (@isset($matches[0]) === TRUE){
					$matches = @preg_replace("/<h1 class=\'post_name\'.*?>/", "", $matches[0]);
					$matches = @preg_replace("/<\/h1>/", "", $matches);
					$post_name = @preg_replace("/[^a-zA-Z0-9] /", "", $matches);
				}
			}
			###Get tags
			$post_tags = '';
			$html_file = @file_get_contents("{$location}{$public}/posts/{$file}");
			$html_file = @str_replace(PHP_EOL, '', $html_file);
			@preg_match('/<ul class=\'post_tags\'.*?<\/ul>/', $html_file, $matches, PREG_OFFSET_CAPTURE);
			if (@isset($matches[0]) === TRUE){
				$matches = $matches[0];
				if (@isset($matches[0]) === TRUE){
					$matches = @preg_replace("/<h1 class=\'post_name\'.*?>/", "", $matches[0]);
					$matches = @preg_replace("/<\/ul>/", "", $matches);
					$matches = @preg_replace("/<ul.*?>/", "", $matches);
					$matches = @preg_replace("/<li.*?>/", "", $matches);
					$matches = @preg_replace("/<\/li>/", ",", $matches);
					$post_tags = @preg_replace("/[^a-zA-Z0-9] ,/", "", $matches);
					$post_tags = @explode(',', $post_tags);
					$post_tags = @array_filter($post_tags);
					foreach ($post_tags as &$item){
						$item = @trim($item);
						$item = "'{$item}'";
					}
					$post_tags = @implode(', ', $post_tags);
				}
			}
			if ($post_tags === ''){
				$post_tags_javascript = 'var entry_tags = null;';
				$post_tags_html = '';
			} else {
				$post_tags_javascript = "var entry_tags = [{$post_tags}];";
				$post_tags_html = @explode(', ', $post_tags);
				foreach ($post_tags_html as &$post_tags_html_item){
					$post_tags_html_item = "<li>{$post_tags_html_item}</li>";
				}
				$post_tags_html = @implode($post_tags_html);
				$post_tags_html = "<ul>{$post_tags_html}</ul>";
				$post_tags_html = str_replace('\'', '', $post_tags_html);
			}
			###Get the post_type. If not found, or invalid, set to blank.
			$post_type = FALSE;
			@preg_match('/<div id=\'post_type\'.*?<\/div>/', file_get_contents("{$location}{$public}/posts/{$file}"), $matches, PREG_OFFSET_CAPTURE);
			if (@isset($matches[0]) === TRUE){
				$matches = $matches[0];
				if (@isset($matches[0]) === TRUE){
					$matches = @preg_replace("/<div id=\'post_type\'.*?>/", "", $matches[0]);
					$matches = @preg_replace("/<\/div>/", "", $matches);
					$post_type = @preg_replace("/[^a-zA-Z0-9] /", "", $matches);
				}
			}
			if ($post_type === 'Article'){
				$post_type = 'article-icon.png';
			} else if ($post_type === 'Media'){
				$post_type = 'media-icon.png';
			} else if ($post_type === 'Download'){
				$post_type = 'download-icon.png';
			} else {
				$post_type = '';
			}
			###Get the post_description. If not found, or invalid, set to blank.
			@preg_match('/<div id=\'post_description\'.*?<\/div>/', file_get_contents("{$location}{$public}/posts/{$file}"), $matches, PREG_OFFSET_CAPTURE);
			if (@isset($matches[0]) === TRUE){
				$matches = $matches[0];
				if (@isset($matches[0]) === TRUE){
					$matches = @preg_replace("/<div id=\'post_description\'.*?>/", "", $matches[0]);
					$matches = @preg_replace("/<\/div>/", "", $matches);
					$post_description = @preg_replace("/[^a-zA-Z0-9]\. /", "", $matches);
				}
			}
			if ($post_description == TRUE){
				#Do nothing
			} else {
				$post_description = '';
			}
			###Write entry to database as javascript object, and store in .js file.
			$data = <<<HEREDOC
//Check if tags for this post match tags in filters, if any
{$post_tags_javascript}
var matches_tags = true;
if (item_tags != null){
	if (entry_tags === null){
		var matches_tags = false;
	} else {
		for (var di = 0; di < item_tags.length; di++) {
			var check = entry_tags.includes(item_tags[di]);
			if (check === false){
				var matches_tags = false;
			}
		} 
	}
}
//Check if search queries match this post_name, if any
var matches_query = true;
if (matches_tags === true){
	var entry_name = "{$post_name}";
	var entry_name = entry_name.split(" ");
	if (item_query != null){
		if (entry_name === null){
			var matches_query = false;
		} else {
			for (var di = 0; di < item_query.length; di++) {
				var check = entry_name.includes(item_query[di]);
				if (check === false){
					var matches_query = false;
				}
			} 
		}
	}
}
//Write current entry data
if (matches_tags === true){
	if (matches_query === true){
		var item = document.createElement("div");
		item.innerHTML = '<div id="entry_type"><img class="entry_icon" src="./assets/{$post_type}"></img></div><a class="entry_name" href="./posts/{$file_id}.html">{$post_name}</a><div id="entry_tags">{$post_tags_html}</div><div id="entry_description">{$post_description}</div>';
		document.getElementById("items").appendChild(item);
		var item_quantity_written = item_quantity_written + 1;
	}
}
//Load next entry script, if applicable
if (item_direction == 'forward'){
	var current_item = current_item + 1;
} else {
	var current_item = current_item - 1;
}
if (current_item > 0){
	if (item_quantity_written < item_quantity){
		var item = document.createElement("script");
		item.src = './databases/database1/' + current_item + '.js';
		document.getElementById("items").appendChild(item);
	}
}
HEREDOC;
			@file_put_contents("{$location}{$public}/databases/database1/{$i}.js", $data);
		}
	}
}
?>